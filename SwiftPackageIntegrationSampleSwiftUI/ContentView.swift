//
//  ContentView.swift
//  SwiftPackageIntegrationSampleSwiftUI
//
//  Created by Vincent Kergonna on 16/11/2022.
//

import SwiftUI
import MiamIOS

struct ContentView: View {
    var body: some View {
        VStack {
            CatalogView()
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
