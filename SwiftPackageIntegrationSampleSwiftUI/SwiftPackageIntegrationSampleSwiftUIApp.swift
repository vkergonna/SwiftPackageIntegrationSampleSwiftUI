//
//  SwiftPackageIntegrationSampleSwiftUIApp.swift
//  SwiftPackageIntegrationSampleSwiftUI
//
//  Created by Vincent Kergonna on 16/11/2022.
//

import SwiftUI
import MiamIOS
import miamCore

@main
struct SwiftPackageIntegrationSampleSwiftUIApp: App {
    init() {
        KoinKt.doInitKoin()
        LogHandler.companion.info("Are you ready ? \(ContextHandlerInstance.shared.instance.isReady())")
        //basketHandler = BasketHandlerInstance.shared.instance
        //basketHandler.setListenToRetailerBasket(func: initBasketListener)
        //basketHandler.setPushProductsToRetailerBasket(func: pushProductToBasket)
        //basketHandler.pushProductsToMiamBasket(retailerBasket: [])
//        PointOfSaleHandler.shared.updateStoreId(storeId: "35290")
        PointOfSaleHandler.shared.setSupplierOrigin(origin:"www.coursesu.fr")
        PointOfSaleHandler.shared.setSupplier(supplierId: 7)
//        PointOfSaleHandler.shared.setSupplier(supplierId: 9)
//        PointOfSaleHandler.shared.setSupplierOrigin(origin: "www.franprix.fr-dev")
        //PointOfSaleHandler.shared.isAvailable = false
        UserHandler.shared.updateUserId(userId: "ed0a471a4bdc755664db84068119144b3a1772d8a6911057a0d6be6a3e075120")
//        Template.sharedInstance.recipeCardTemplate = { (recipeCardViewModel: RecipeCardVM,
//                                                        look: @escaping () -> Void,
//                                                        buy: @escaping () -> Void ) -> AnyView in
    }
    
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

